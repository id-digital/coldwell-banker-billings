<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package findeo
 */

?>

<!-- Footer
================================================== -->
<?php 
$sticky = get_option('findeo_sticky_footer') ;
$style = get_option('findeo_footer_style') ;

if(is_singular()){

	$sticky_singular = get_post_meta($post->ID, 'findeo_sticky_footer', TRUE); 
	
	switch ($sticky_singular) {
		case 'on':
		case 'enable':
			$sticky = true;
			break;

		case 'disable':
			$sticky = false;
			break;	

		case 'use_global':
			$sticky = get_option('findeo_sticky_footer'); 
			break;
		
		default:
			$sticky = get_option('findeo_sticky_footer'); 
			break;
	}

	$style_singular = get_post_meta($post->ID, 'findeo_footer_style', TRUE); 
	switch ($style_singular) {
		case 'light':
			$style = 'light';
			break;

		case 'dark':
			$style = 'dark';
			break;

		case 'use_global':
			$style = get_option('findeo_footer_style'); 
			break;
		
		default:
			$style = get_option('findeo_footer_style'); 
			break;
	}
}

$sticky = apply_filters('findeo_sticky_footer_filter',$sticky);
?>
<div id="footer" class="<?php echo esc_attr($style); echo ($sticky == 'on' || $sticky == 1 || $sticky == true) ? " sticky-footer" : ''; ?> ">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<?php
			$footer_layout = get_option( 'pp_footer_widgets','6,3,3' ); 
			
	        $footer_layout_array = explode(',', $footer_layout); 
	        $x = 0;
	        foreach ($footer_layout_array as $value) {
	            $x++;
	             ?>
	             <div class="col-md-<?php echo esc_attr($value); ?> col-sm-6 col-xs-12">
	                <?php
					if( is_active_sidebar( 'footer'.$x ) ) {
						dynamic_sidebar( 'footer'.$x );
					}
	                ?>
	            </div>
	        <?php } ?>

		</div>
		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights"> <?php $copyrights = get_option( 'pp_copyrights' , '&copy; Theme by Purethemes.net. All Rights Reserved.' ); 
		
		        if (function_exists('icl_register_string')) {
		            icl_register_string('Copyrights in footer','copyfooter', $copyrights);
		            echo icl_t('Copyrights in footer','copyfooter', $copyrights);
		        } else {
		            echo wp_kses($copyrights,array( 'a' => array('href' => array(),'title' => array()),'br' => array(),'em' => array(),'strong' => array(),));
										 } ?></div>
				<div class="idd-logo">
					<a target="_blank" href="https://www.iddigital.us/">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 29" style="enable-background:new 0 0 100 29;" xml:space="preserve" width="120" height="34">
						<g>
							<rect y="8.1" class="st0" width="2.9" height="14.4"></rect>
							<path class="st0" d="M10.8,8.1H6v14.4h4.6c3.8,0,4.9-2.3,4.9-7.4C15.5,10.8,14.8,8.1,10.8,8.1z M10.2,20.3H8.9V10.2h1.2 c2.2,0,2.5,1.7,2.5,4.9C12.6,19,12.2,20.3,10.2,20.3z"></path>
							<path class="st0" d="M23.3,12.9L23.3,12.9c-0.4-0.9-1.4-1.5-2.4-1.5c-3,0-3.4,3.4-3.4,5.7c0,3.4,1,5.7,3.5,5.7 c1.2,0,2.1-0.8,2.3-1.6h0v1.3H25V8.1h-1.7V12.9z M21.3,21.3c-1.6,0-2-1.7-2-4.2s0.4-4.2,2-4.2c1.8,0,2.2,2.1,2.2,4.2 C23.4,19.5,23.1,21.3,21.3,21.3z"></path>
							<rect x="27.3" y="11.7" class="st0" width="1.7" height="10.8"></rect>
							<rect x="27.3" y="8.1" class="st0" width="1.7" height="1.9"></rect>
							<path class="st0" d="M36.8,13L36.8,13c-0.3-0.8-1.2-1.7-2.4-1.7c-2.7,0-3.5,2.8-3.5,5.7c0,1.4,0.2,5.4,3.3,5.4 c1.1,0,2.1-0.5,2.4-1.4h0v1.4c0,0.7,0.1,2.4-2.1,2.4c-0.9,0-1.7-0.4-1.7-1.4h-1.7c0.3,2.7,2.9,2.7,3.5,2.7c2.2,0,3.7-1.2,3.7-4.4 V11.7h-1.6V13z M34.7,21c-1.7,0-2-2.2-2-3.9c0-2.6,0.4-4.2,2-4.2c1.8,0,2.2,2.1,2.2,4.2C36.9,19.3,36.5,21,34.7,21z"></path>
							<rect x="40.8" y="11.7" class="st0" width="1.7" height="10.8"></rect>
							<rect x="40.8" y="8.1" class="st0" width="1.7" height="1.9"></rect>
							<path class="st0" d="M47.4,8.5h-1.7v3.1h-1.6V13h1.6v7.4c0,1.4,0.4,2.1,2.2,2.1c0.7,0,1.1-0.1,1.4-0.1v-1.4c-0.2,0-0.4,0.1-0.7,0.1 c-0.9,0-1.1-0.3-1.1-1.2V13h1.9v-1.3h-1.9V8.5z"></path>
							<path class="st0" d="M57.4,20.5v-6c0-1.1,0-3.1-3.3-3.1c-2.6,0-3.6,1.1-3.6,3.5h1.7v-0.1c0-0.4,0-2.1,1.9-2.1 c1.1,0,1.7,0.6,1.7,1.6c0,0.9,0,1.2-1.2,1.6l-2.3,0.7c-1.4,0.4-2,1.5-2,3c0,1.9,0.9,3.1,2.9,3.1c1,0,2.1-0.6,2.6-1.6h0 c0.1,1.4,0.9,1.4,1.5,1.4c0.4,0,0.9-0.1,1-0.1v-1.2c-0.1,0.1-0.2,0.1-0.4,0.1C57.4,21.4,57.4,20.9,57.4,20.5z M55.7,19.3 c0,1.1-1.1,2.1-2.2,2.1c-1.1,0-1.5-0.8-1.5-1.9c0-0.9,0.3-1.4,0.8-1.7c0.8-0.5,2.3-0.6,2.9-1.3V19.3z"></path>
							<rect x="59.8" y="8.1" class="st0" width="1.7" height="14.4"></rect>
							<polygon class="st0" points="77.6,4.9 77.6,11.2 80.9,14.5 77.6,17.8 77.6,24.1 87.2,14.5 	"></polygon>
							<path class="st0" d="M86.5,0.5h-6.5l14,14l-14,14h6.5c8,0,13.5-6.1,13.5-14v-0.1C100,6.5,94.5,0.5,86.5,0.5z"></path>
							<polygon class="st0" points="66.9,24.7 74.5,17.1 74.5,11.9 66.9,4.3 	"></polygon>
							<polygon class="st0" points="74.5,8.1 74.5,0.5 66.9,0.5 	"></polygon>
							<polygon class="st0" points="74.5,28.5 74.5,20.9 66.9,28.5 	"></polygon>
						</g>
					</svg>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

</div> <!-- weof wrapper -->
<?php wp_footer(); ?></body>
</html>
