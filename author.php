<?php
/**
 * The template for displaying author archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package findeo
 */

get_header(get_option('header_bar_style','standard') ); 
$template_loader = new Realteo_Template_Loader; 

$agent = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
$agent_info = get_userdata( $agent->ID );
$email = $agent_info->user_email;
?>
<!-- Titlebar
================================================== -->
<div id="titlebar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h1><?php echo esc_html($agent_info->first_name); ?> <?php echo esc_html($agent_info->last_name); ?></h1>
                <?php if( isset( $agent_info->agent_title ) ) : ?><span><?php echo esc_html($agent_info->agent_title); ?></span><?php endif; ?>

            </div>
        </div>
    </div>
</div>

<!-- Content
================================================== -->
<div class="container">
	<div class="row">

		<div class="col-md-12">
			<div class="agent agent-page">

				<div class="agent-avatar">
					<?php agent_photo($agent->ID); ?>
				</div>

				<div class="agent-content">
					<div class="agent-name">
						<h4><?php echo esc_html($agent_info->first_name); ?> <?php echo esc_html($agent_info->last_name); ?></h4>
						<span><?php echo esc_html($agent_info->agent_title); ?></span>
					</div>

					<p>
						<?php 
					 	$allowed_tags = wp_kses_allowed_html( 'post' );
              			echo wp_kses($agent->description,$allowed_tags);
   						?>
   					</p>

					<ul class="agent-contact-details">
						<?php if(isset($agent_info->phone) && !empty($agent_info->phone)): ?>
							<li><i class="sl sl-icon-call-in"></i><?php echo esc_html($agent_info->phone); ?></li>
						<?php endif; ?>
						<?php if(isset($agent_info->user_email)): ?>
							<li><i class="fa fa-envelope-o "></i><a href="mailto:<?php echo esc_attr($email);?>"><?php echo esc_html($email);?></a></li>
						<?php endif; ?>
					</ul>

					<ul class="social-icons">
						<?php
							$socials = array('facebook','twitter','gplus','linkedin');
							foreach ($socials as $social) {
								$social_value = get_user_meta($agent->ID, $social, true);
								if(!empty($social_value)){ ?>
									<li><a class="<?php echo esc_attr($social); ?>" href="<?php echo esc_url($social_value); ?>"><i class="icon-<?php echo esc_attr($social); ?>"></i></a></li>
								<?php }
							}
						?>
					</ul>
					<div class="clearfix"></div>
				</div>

			</div>
		</div>

	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="row sticky-wrapper">

		<div class="col-md-8">
			

			<div class="row margin-bottom-15">
				<?php do_action( 'realto_before_archive' ); ?>
			</div>

			
				<!-- Listings -->
				<div class="listings-container list-layout">
				<?php

                $args1 = array(
                    'post_type' => 'property',
                    'meta_key' => 'additional_agents',
                    'meta_value' => $agent->ID,
                    'meta_compare' => 'LIKE',
                    'nopaging' => true
                );
                $args2 = array(
                    'post_type' => 'property',
                    'author' => $agent->ID,
                    'nopaging' => true
                );
                $posts = array_merge(get_posts($args1), get_posts($args2));
                $post_ids = array_map(function($post) {
                    return $post->ID;
                }, $posts);
                if ( $post_ids ) {
                    $final_args = [
                        'nopaging' => true,
                        'post_type' => 'property',
                        'post__in' => $post_ids
                    ];
                    $order = get_query_var('realteo_order');
                    if ($order === 'price-asc' || $order === 'price-desc') {
                        $final_args['orderby'] = 'meta_value';
                        $final_args['meta_key'] = '_price';
                        $final_args['order'] = ($order === 'price-asc') ? 'ASC' : 'DESC';
                    }
                    else if ($order === 'date-asc' || $order === 'date-desc') {
                        $final_args['orderby'] = 'date';
                        $final_args['order'] = ($order === 'date-asc') ? 'ASC' : 'DESC';
                    }
                    else if ($order === 'featured') {
                        $final_args['orderby'] = 'date';
                        $final_args['order'] = 'DESC';
                    }
                    else if ($order === 'rand') {
                        $final_args['orderby'] = 'rand';
                    }
                    $loop_posts = get_posts($final_args);
                    if ($order === 'featured') {
                        usort($loop_posts, function($a, $b) {
                            if ($a->_featured == 'on' && empty($b->_featured)) {
                                return 0;
                            } elseif ($a->_featured == 'on' && $b->_featured == 'on' && $a->post_date > $b->post_date) {
                                return 0;
                            } elseif (empty($a->_featured) && empty($b->_featured) && $a->post_date > $b->post_date) {
                                return 0;
                            } else {
                                return 1;
                            }
                        });
                    }

                    if (!empty($loop_posts)) :
                        /* Start the Loop */
                        foreach($loop_posts as $this_post) :
                            global $post;
                            $post = $this_post;
                            setup_postdata($post);
                            /*
                             * Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            $template_loader->get_template_part('content-property');

                        endforeach;
                        wp_reset_postdata();

                        the_posts_navigation();

                    else :

                        get_template_part('template-parts/author-content-none');

                    endif;
                } else
                    get_template_part('template-parts/author-content-none');
                ?>
				</div>
				<div class="margin-top-60"></div>
		</div>
		<!-- Sidebar
		================================================== -->
		<div class="col-md-4">
			<?php $template_loader->get_template_part( 'sidebar-realteo' );?>
		</div>
	<!-- Sidebar / End -->
	</div>
</div>

<?php get_footer(); ?>
