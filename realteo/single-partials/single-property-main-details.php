<!-- Main Details -->
<?php 
$details_list = Realteo_Meta_Boxes::meta_boxes_main_details(); 

$class = (isset($data->class)) ? $data->class : 'property-main-features' ;
?>
<ul class="<?php esc_attr_e($class); ?>">
<?php
$property_type = get_post_meta($post->ID, '_property_type', true);	
foreach ($details_list['fields'] as $detail => $value) {
	if ($value['id'] != '_property_type')
		$meta_value = get_post_meta($post->ID, $value['id'],true);
	else
		$meta_value = $property_type;
	
	if(!empty($meta_value)) {
		if($value['id'] == '_area'){
			$scale = realteo_get_option( 'scale', 'sq ft' );
			if ($scale == 'sq ft' && $property_type == 'land')
				$scale = 'acres';
			echo '<li class="main-detail-'.$value['id'].'">'.apply_filters('realteo_scale',$scale).' <span>'.$meta_value.'</span> </li>';
		} elseif ($value['id'] == '_brochure' && is_single()) {
			echo '<li class="main-detail-'.$value['id'].'"> <span><a href="'.$meta_value.'" target="_blank">Brochure</a></span> </li>';	
		} elseif (isset($details_list['fields'][$detail]['options']) && !empty($details_list['fields'][$detail]['options'])) {
			echo '<li class="main-detail-'.$value['id'].'">'. $value['name'].' <span>'.$details_list['fields'][$detail]['options'][$meta_value].'</span> </li>';	
		} else {
			echo '<li class="main-detail-'.$value['id'].'">'. $value['name'].' <span>'.$meta_value.'</span> </li>';	
		}
	}
}
?>
</ul>
