<?php

add_action( 'wp_enqueue_scripts', 'findeo_enqueue_styles', 20 );
function findeo_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css',array('bootstrap','findeo-icons','findeo-woocommerce') );
	wp_dequeue_style( 'findeo-style' );
    wp_enqueue_style( 'findeo-style', get_template_directory_uri() . '/style.css', array('bootstrap','findeo-icons','findeo-woocommerce', 'parent-style'), '20180504' );
}

 
function remove_parent_theme_features() {
}
add_action( 'after_setup_theme', 'remove_parent_theme_features', 10 );

function idd_highlights_section($content)
{
	global $post;

	$highlights = get_post_meta($post->ID, '_highlights', true);
	if (is_single() && get_post_type($post) == 'property' && !empty($highlights))
	{
		$content .= '<h3 class="desc-headline">Highlights</h3>';
		$content .= '<p>' . html_entity_decode(nl2br($highlights)) . '</p>';
	}
	return $content;
}
add_filter('the_content', 'idd_highlights_section', 15);

function idd_suites_section($content)
{
	global $post;

	$suites = get_post_meta($post->ID, '_suites', true);
	if (is_single() && get_post_type($post) == 'property' && !empty($suites))
	{
		$content .= '<h3 class="desc-headline">Suites Available</h3>';
		$content .= '<p>' . html_entity_decode(nl2br($suites)) . '</p>';
	}
	return $content;
}
add_filter('the_content', 'idd_suites_section', 16);

function idd_brochure_section($content)
{
	global $post;
	
	$link = get_post_meta($post->ID, '_brochure', true);
	if (is_single() && get_post_type($post) == 'property' && !empty($link))
	{
		$content .= '<h3 class="desc-headline">Brochure </h3>';
		$content .= '<a target="_blank" href="' . $link . '">View Brochure</a>';
	}
	return $content;
}
add_filter('the_content', 'idd_brochure_section', 17);

function filter_out_closed_props($query)
{
	if (is_admin() || ! $query->is_main_query() )
		return $query;
	if ( is_post_type_archive('property') || is_author() )
	{
		$query->set('posts_per_page', 50);
		
		$noclosed_query = array (
			'key' => '_offer_type',
			'value' => 'closed',
			'compare' => '!='
		);
		$meta_queries = $query->get('meta_query');
		if (!empty($meta_queries))
		{
			$meta_queries[] = $noclosed_query;
			$meata_queries['relation'] = 'AND';
		}
		else
			$meta_queries = array($noclosed_query);
		$query->set('meta_query', $meta_queries);
	}
	return $query;
}
add_action('pre_get_posts', 'filter_out_closed_props', 1);

function idd_property_archive_description($description)
{
	$offer_type = get_query_var('_offer_type');
    if (is_post_type_archive( 'property' ) && $offer_type == 'sale' ) {
		$description = "Looking for a commercial property to purchase? Search for an investment property, office space, retail, specialty store, shopping centers, hospitality, healthcare, sports and entertainment, land or industrial space. Click here for more details.";
	}
    if (is_post_type_archive( 'property' ) && $offer_type == 'rent' ) {
		$description = "Looking for a commercial property to lease? Search for an investment property, office space, retail, specialty store, shopping centers, hospitality, healthcare, sports and entertainment, land or industrial space. Click here for more details.";		
	}
	return $description;
}
add_filter('wpseo_metadesc', 'idd_property_archive_description', 100);

function idd_property_archive_title($title)
{
	$offer_type = get_query_var('_offer_type');
    if (is_post_type_archive( 'property' ) && $offer_type == 'sale' ) {
		$title = "Find A Property For Sale | Coldwell Banker Commercial CBS";
	}
    if (is_post_type_archive( 'property' ) && $offer_type == 'rent' ) {
		$title = "Find A Property For Lease | Coldwell Banker Commercial CBS";
	}
	return $title;
}
add_filter('wpseo_title', 'idd_property_archive_title', 100);

function agent_photo($agent_id)
{
	$attachment_id = get_user_meta($agent_id, 'realteo_avatar_id', true);
	$attr = array(
		'class' => 'avatar photo'
	);
	$image = wp_get_attachment_image($attachment_id, 'large', false, $attr);
	echo $image;
}

// Hooks near the bottom of profile page (if current user) 
add_action('show_user_profile', 'custom_user_profile_fields');

// Hooks near the bottom of the profile page (if not current user) 
add_action('edit_user_profile', 'custom_user_profile_fields');

// @param WP_User $user
function custom_user_profile_fields( $user ) {
?>
    <table class="form-table">
        <tr>
            <th>
                <label for="user_order"><?php _e( 'User Order' ); ?></label>
            </th>
            <td>
                <input type="number" name="user_order" id="user_order" value="<?php echo esc_attr( get_the_author_meta( 'user_order', $user->ID ) ); ?>" class="regular-text" />
            </td>
        </tr>
    </table>
<?php
}


// Hook is used to save custom fields that have been added to the WordPress profile page (if current user) 
add_action( 'personal_options_update', 'update_extra_profile_fields' );

// Hook is used to save custom fields that have been added to the WordPress profile page (if not current user) 
add_action( 'edit_user_profile_update', 'update_extra_profile_fields' );

function update_extra_profile_fields( $user_id ) {
    if ( current_user_can( 'edit_user', $user_id ) )
        update_user_meta( $user_id, 'user_order', $_POST['user_order'] );
}


Class IDD_Realteo_Widget extends WP_Widget {
    /**
     * Widget CSS class
     *
     * @access public
     * @var string
     */
    public $widget_cssclass;

    /**
     * Widget description
     *
     * @access public
     * @var string
     */
    public $widget_description;

    /**
     * Widget id
     *
     * @access public
     * @var string
     */
    public $widget_id;

    /**
     * Widget name
     *
     * @access public
     * @var string
     */
    public $widget_name;

    /**
     * Widget settings
     *
     * @access public
     * @var array
     */
    public $settings;

    /**
     * Constructor
     */
    public function __construct() {
        $this->register();

    }


    /**
     * Register Widget
     */
    public function register() {
        $widget_ops = array(
            'classname'   => $this->widget_cssclass,
            'description' => $this->widget_description
        );

        parent::__construct( $this->widget_id, $this->widget_name, $widget_ops );

        add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
        add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
        add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );


    }



    /**
     * get_cached_widget function.
     */
    public function get_cached_widget( $args ) {
        $cache = wp_cache_get( $this->widget_id, 'widget' );

        if ( ! is_array( $cache ) )
            $cache = array();

        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return true;
        }

        return false;
    }

    /**
     * Cache the widget
     */
    public function cache_widget( $args, $content ) {
        $cache[ $args['widget_id'] ] = $content;

        wp_cache_set( $this->widget_id, $cache, 'widget' );
    }

    /**
     * Flush the cache
     * @return [type]
     */
    public function flush_widget_cache() {
        wp_cache_delete( $this->widget_id, 'widget' );
    }

    /**
     * update function.
     *
     * @see WP_Widget->update
     * @access public
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        if ( ! $this->settings )
            return $instance;

        foreach ( $this->settings as $key => $setting ) {
            $instance[ $key ] = sanitize_text_field( $new_instance[ $key ] );
        }

        $this->flush_widget_cache();

        return $instance;
    }

    /**
     * form function.
     *
     * @see WP_Widget->form
     * @access public
     * @param array $instance
     * @return void
     */
    function form( $instance ) {

        if ( ! $this->settings )
            return;

        foreach ( $this->settings as $key => $setting ) {

            $value = isset( $instance[ $key ] ) ? $instance[ $key ] : $setting['std'];

            switch ( $setting['type'] ) {
                case 'text' :
                    ?>
                    <p>
                        <label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $setting['label']; ?></label>
                        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( $key ) ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" type="text" value="<?php echo esc_attr( $value ); ?>" />
                    </p>
                    <?php
                    break;
                case 'checkbox' :
                    ?>
                    <p>
                        <label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $setting['label']; ?></label>
                        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( $key ) ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" type="checkbox" <?php checked( esc_attr( $value ), 'on' ); ?> />
                    </p>
                    <?php
                    break;
                case 'number' :
                    ?>
                    <p>
                        <label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $setting['label']; ?></label>
                        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( $key ) ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" type="number" step="<?php echo esc_attr( $setting['step'] ); ?>" min="<?php echo esc_attr( $setting['min'] ); ?>" max="<?php echo esc_attr( $setting['max'] ); ?>" value="<?php echo esc_attr( $value ); ?>" />
                    </p>
                    <?php
                    break;
                case 'dropdown' :
                    ?>
                    <p>
                        <label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $setting['label']; ?></label>
                        <select class="widefat" id="<?php echo esc_attr( $this->get_field_id( $key ) ); ?>" name="<?php echo $this->get_field_name( $key ); ?>">

                            <?php foreach ($setting['options'] as $option_key => $option_value) { ?>
                                <option <?php selected($value,$option_key); ?> value="<?php echo esc_attr($option_key); ?>"><?php echo esc_attr($option_value); ?></option>
                            <?php } ?></select>

                    </p>
                    <?php
                    break;
            }
        }
    }

    /**
     * widget function.
     *
     * @see    WP_Widget
     * @access public
     *
     * @param array $args
     * @param array $instance
     *
     * @return void
     */
    public function widget( $args, $instance ) {}
}


class IDD_Realteo_Contact_Agent_Widget extends IDD_Realteo_Widget {
    /**
     * Constructor
     */
    public function __construct() {
        global $wp_post_types;

        $this->widget_cssclass    = 'realteo widget_contact_agent';
        $this->widget_description = __( 'Display a Contact form.', 'realteo' );
        $this->widget_id          = 'widget_idd_contact_widget_findeo';
        $this->widget_name        =  __( 'IDD Findeo Contact Widget', 'realteo' );
        $this->settings           = array(
            'phone' => array(
                'type'  => 'checkbox',
                'std'	=> 'on',
                'label' => __( 'Show phone number', 'realteo' )
            ),
            'email' => array(
                'type'  => 'checkbox',
                'std'	=> 'on',
                'label' => __( 'Show email', 'realteo' )
            ),
            'desc' => array(
                'type'  => 'checkbox',
                'std'	=> 'on',
                'label' => __( 'Show agent description', 'realteo' )
            ),
            'contact' => array(
                'type'  => 'dropdown',
                'std'	=> '',
                'options' => $this->get_forms(),
                'label' => __( 'Choose contact form', 'realteo' )
            ),
        );
        $this->register();

        //add_filter( 'wpcf7_mail_components', array( $this, 'set_question_form_recipient' ), 10, 3 );

    }

    /**
     * widget function.
     *
     * @see WP_Widget
     * @access public
     * @param array $args
     * @param array $instance
     * @return void
     */
    public function widget( $args, $instance ) {
        if ( $this->get_cached_widget( $args ) ) {
            return;
        }

        ob_start();

        extract( $args );
        /*
        $print = $instance['print'];
        $save = $instance['save'];*/
        echo $before_widget;
        $agents[] = get_user_by('id', get_the_author_meta( 'ID' ));
        $additional_owners = get_field('additional_agents');
        if ($additional_owners) {
            $additional_owners = array_map(function ($agent) {
                return get_user_by('id', $agent['ID']);
            }, $additional_owners);
            $agents = array_merge($agents, $additional_owners);
            $agents = array_filter($agents);
        }
        if (!empty($agents)) :
            ?>
            <!-- Agent Widget -->
            <div class="agent-widget">
                <?php foreach($agents as $agent) : ?>
                    <div class="agent-title">
                        <div class="agent-photo"><?php echo get_avatar( $agent->ID, 72 );  ?></div>
                        <div class="agent-details">
                            <h4><a href="<?php echo esc_url(get_author_posts_url( $agent->ID )); ?>"><?php echo $agent->first_name; ?> <?php echo $agent->last_name; ?></a></h4>
                            <?php
                            if(isset($instance['phone']) && !empty($instance['phone'])) {
                                if(isset($agent->phone) && !empty($agent->phone)): ?><span><i class="sl sl-icon-call-in"></i><a href="tel:<?php echo esc_html($agent->phone); ?>"><?php echo esc_html($agent->phone); ?></a></span><?php endif;
                            }
                            if(isset($instance['email']) && !empty($instance['email'])) {
                                if(isset($agent->user_email)): $email = $agent->user_email; ?>
                                    <br><span><i class="fa fa-envelope-o "></i><a href="mailto:<?php echo esc_attr($email);?>"><?php echo esc_html($email);?></a></span>
                                <?php endif; ?>
                            <?php } ?>
                        </div>

                        <div class="clearfix"></div>


                    </div>
                    <?php if(isset($instance['desc']) && !empty($instance['desc'])) {  ?>
                        <div class="agent-desc">
                            <?php
                            $allowed_tags = wp_kses_allowed_html( 'post' );
                            echo wp_kses($agent->description,$allowed_tags);
                            ?>
                        </div>
                    <?php }
                endforeach;
                if(isset($instance['contact']) && !empty($instance['contact'])) {
                    echo do_shortcode( sprintf( '[contact-form-7 id="%s"]', $instance['contact'] ) );
                } ?>
            </div>

            <!-- Agent Widget / End -->
        <?php
        endif;
        echo $after_widget;

        $content = ob_get_clean();

        echo $content;

        $this->cache_widget( $args, $content );
    }

    public function get_forms() {
        $forms  = array( 0 => __( 'Please select a form', 'realteo' ) );

        $_forms = get_posts(
            array(
                'numberposts' => -1,
                'post_type'   => 'wpcf7_contact_form',
            )
        );

        if ( ! empty( $_forms ) ) {

            foreach ( $_forms as $_form ) {
                $forms[ $_form->ID ] = $_form->post_title;
            }
        }

        return $forms;
    }

}

unregister_widget('Realteo_Contact_Agent_Widget');
register_widget('IDD_Realteo_Contact_Agent_Widget');


function idd_custom_get_post_author_email($atts){
    $value = '';
    global $post;
    $post_id = $post->ID;
    $object = get_post( $post_id );
    //just get the email of the listing author
    $owner_ID = $object->post_author;
    //retrieve the owner user data to get the email
    $owner_info = get_userdata( $owner_ID );
    if ( false !== $owner_info ) {
        $value = $owner_info->user_email;
    }
    $secondary_owners = get_field('additional_agents', $post_id);
    if ($secondary_owners) {
        foreach ($secondary_owners as $secondary) {
            $owner_info = get_userdata($secondary['ID']);
            if (false !== $owner_info) {
                $value .= ', ' . $owner_info->user_email;
            }
        }
    }
    return $value;
}
add_shortcode('IDD_CUSTOM_POST_AUTHOR_EMAIL', 'idd_custom_get_post_author_email');